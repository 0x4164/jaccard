<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once("SimpleHTTPClient.php");

/**
 * 
 * sinkaPay lib utk akses webservice kemenkeu
 * @0x4164
 * 
 * requires :
 * - SimpleHTTPClient : https://github.com/ekillaby/simple-http-client
 * 
 * to do : 
 * - add exception
 * - Log
 * 
 */

class SinonimKata{
    protected $_ci;
    
    // dev
    // aka wsdev.sinka.kemenkeu.go.id
    const BASE_URL = "http://www.sinonimkata.com/";

    //dummy
    // const BASE_URL = "http://localhost/sinka/index.php";

    //prod
    // aka ws.sinka.kemenkeu.go.id
    // const BASE_URL = "http://10.242.17.238/index.php/api/sinka/index/format/json/";
    
    //endpoints / methods
    //create billing
    const GET_SYNONIM = self::BASE_URL."search.php";

    public $data;

	function __construct(){
		$this->_ci =&get_instance();
    }
    
	function setData($data){
        $this->data = $data;

        return $this;
    }
    
	function getData(){
        return $this->data;
    }

	function getStandardHeader(){
        return [
            'Connection: keep-alive',
            'Cache-Control: max-age=0',
            'Origin: http://www.sinonimkata.com',
            'Upgrade-Insecure-Requests: 1',
            'Content-Type: application/x-www-form-urlencoded',
            'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.74 Safari/537.36',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Referer: http://www.sinonimkata.com/sinonim-160566-pendaftaran.html',
            'Accept-Encoding: gzip, deflate',
            'Accept-Language: en-US,en;q=0.9,ja;q=0.8'
        ];
    }

	private function sinkaPost($url, $payload, $header){
        $client = new SimpleHTTPClient();
        $ret = $client->post($url, 
            $payload, $header
        );

        return $ret;
    }

	private function sinkaGet($url, $payload){
        $client = new SimpleHTTPClient();
        $ret = $client->get($url, 
            $payload
        );

        return $ret;
    }

	function domParser($content = ""){
        $dom = new DomDocument();
        // surpress warning
        @$dom->loadHTML($content);
        // $xpath = new DOMXpath($dom);
        
        return $dom;
    }

	function getSynonim($word = null){
        $header = $this->getStandardHeader();
        // preout(self::GET_SYNONIM);
        $ret = $this->sinkaPost(self::GET_SYNONIM, "q=".strtolower($word), $header);
        // toFile(presonRet(json_decode($ret["body"])), "dummySPCreate");
        $dom = $this->domParser($ret['body']);
        $body = $dom->getElementsByTagName('td');
        $syn = $body[2]->nodeValue; 

        return $syn;
    }
}