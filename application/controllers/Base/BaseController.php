<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

class BaseController extends CI_Controller {
	protected $posts;
	protected $gets;

	function __construct()
	{
		parent:: __construct();
	}

	public function sessionCheck(){
		$loggedIn = $this->access->is_login();
		if(! $loggedIn){
			redirect(site_url('auth/login'));
			return;
		}
	}

	public function posts(){
		return $this->input->post();
	}

	public function back(){
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function index(){
		redirect('page/home');
	}

	public function withScript($scriptfile = null){
		$scriptfile = $scriptfile === null ? "admin/scripts/dashboard" : $scriptfile;
		return $this->template->setScriptFile($scriptfile);
	}

	public function create(){
		
	}

	public function edit(){
		
	}

	public function update(){
		
	}

	public function delete($id){
		
	}
}