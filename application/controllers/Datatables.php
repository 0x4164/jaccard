<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(__DIR__.'/../helpers/Constants.php');
require_once(__DIR__.'/Base/BaseController.php');

class Datatables extends BaseController {
	 
	function __construct()
	{
		parent:: __construct();
		$this->load->library('template');
		date_default_timezone_set('Asia/Jakarta');
		// $this->load->library('access');
		// $this->load->model('db_model');
		// $this->sessionCheck();
	}

	public function loadAndRender($modelName = "") {
		$this->load->model($modelName, 'dtmdl');
		$this->dtmdl->getRendered();
	}

	public function proses($courseId = 0, $type = "") {
		$_GET['courseId'] = $courseId;
		
		switch($type){
			case "final-sim":
				$this->loadAndRender("Datatables/DataResFinal");
			break;
			case "ctx-sim":
				$this->loadAndRender("Datatables/DataResCtx");
			break;
			case "ctx-cfragment":
				$this->loadAndRender("Datatables/DataResCommonFragment");
			break;
			case "subproses":
				$this->load->model('Datatables/DataSubproses', 'dtSubproses');
				$this->dtPesertaDiklat->getRendered();
			case "sinonim":
				$this->load->model('Datatables/DataSinonim', 'dtSinonim');
				$this->dtSinonim->getRendered();
			break;
			case "rpa":
				$this->load->model('Datatables/DataRPA', 'dtRpa');
				$this->dtRpa->getRendered();
			break;
			default:
				$this->load->model('Datatables/DataProses', 'dtProses');
				$this->dtProses->getRendered();
		}
	}
}