<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
	 
	function __construct()
	{
		parent:: __construct();
		$this->current_db = $this->load->database('default',TRUE);
		
		$this->load->library('form_validation');
		$this->load->library('template');
		$this->load->library('access');
	}
	
	// ------------------------- START LOGIN CODE ------------------------- //
	
	public function index()
	{
		// $this->load->library('form_validation');
		// $this->load->helper('form');
		
		// $this->form_validation->set_rules('username','Username','trim|required|strip_tags');
		// $this->form_validation->set_rules('password','Password','trim|strip_tags');
		// $this->form_validation->set_rules('token','token','callback_check_login');
		
		// if($this->access->is_login()==FALSE){
		// 	if($this->form_validation->run() == FALSE){
		 		$this->template->admin('login');
		// 	}else{
		// 		redirect('page');
		// 	}
		// }else{
		// 	redirect('page');
		// }
	}
	
	public function login(){
		$this->template->admin('login');
	}

	public function loginSubmit()
	{
		$username = $this->input->post('username',TRUE);
		$password = $this->input->post('password',TRUE);
		
		$login = $this->access->login($username,$password);
		
		if($login){
			redirect(base_url('admin'));
			return TRUE;
		}else{
			$this->form_validation->set_message('check_login','Username Or Password is Invalid');
			redirect(base_url('auth/login'));
			return FALSE;
		}
	}
	
	function logout()
	{
		$this->access->logout();
		redirect('main');
	}
	
	// ------------------------- END LOGIN ------------------------- //
	
}

/* End of file monitoring.php */
/* Location: ./application/controllers/monitoring.php */