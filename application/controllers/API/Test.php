<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(__DIR__.'/../../helpers/Constants.php');
require_once(__DIR__.'/../Base/BaseController.php');

class Test extends BaseController {
	 
	function __construct(){
		parent:: __construct();
		$this->load->library('template');
		date_default_timezone_set('Asia/Jakarta');
		$this->load->library('access');
		$this->load->model('db_model');
	}
	
	public function index(){
		$apiKeys = [
			"api-key-x7a616d"
		];
		
		$msg = "API key match";
		$data = null;
		$header = getHeaderList();

		$xapiKey = arrayGet($header, 'X-Api-Key');
		
		if($xapiKey == null || !in_array($xapiKey, $apiKeys)){
			$msg = "Invalid api key";
			jsonResponse(401, $msg, $data);
			return;
		}

		jsonResponse(200, $msg, $data);
	}

	public function test(){
		
	}
}