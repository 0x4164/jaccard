<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(__DIR__.'/../helpers/Constants.php');
require_once(__DIR__.'/Base/BaseController.php');

class Home extends BaseController {
	 
	function __construct()
	{
		parent:: __construct();
		$this->load->library('template');
		date_default_timezone_set('Asia/Jakarta');
		// $this->load->library('access');
		// $this->load->model('db_model');
	}
	
	public function index(){
		$data['tahun'] = 2020;
		$data['data'] = [];
		$this->withScript("admin/scripts/index")
			->admin('admin/index', $data);
	}

	public function result($type = null){
		$data['data'] = [];

		switch($type){
			case "cluster-sim":
				$this->withScript("admin/scripts/result-cluster")
					->admin('admin/result-cluster', $data);
			break;
			case "contextual-sim":
				$this->withScript("admin/scripts/result-ctxsim")
					->admin('admin/result-ctxsim', $data);
			break;
			case "common-fragment":
				$this->withScript("admin/scripts/result-cfragment")
					->admin('admin/result-cfragment', $data);
			case "sinonim":
				$this->withScript("admin/scripts/result-sinonim")
					->admin('admin/result-sinonim', $data);
			break;
			case "rpa":
				$data['type'] = "rpa";
				$data['rpa'] = [];
				$this->withScript("admin/scripts/result-rpa")
					->admin('admin/result-rpa', $data);
			break;
			case "rpa-group":
				$this->load->model('ResultModel', 'resultmodel');
				$data['type'] = "rpa-group";
				$data['rpa'] = $this->resultmodel->getRPA();
				$this->withScript("admin/scripts/result-rpa")
					->admin('admin/result-rpa', $data);
			break;
			case "all":
			default:
				$this->withScript("admin/scripts/result")
					->admin('admin/result', $data);
		}
	}
}