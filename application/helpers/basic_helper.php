<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function csrf($get)
{
	$CI =&get_instance();

	if($get == 'name')
	{
		return $CI->security->get_csrf_token_name();
	}elseif ($get == 'hash') {
		return $CI->security->get_csrf_hash();
	}else{
		return false;
	}
}

// to do : refactor
///////echo if variable is avaliable and not null
function setval(&$data, $output = '', $replace_null = false)
{
	if($replace_null){
		if(isset($data) && $data != '')
		{
			return $data;
		}else{
			return $output;
		}
	}else{
		if(isset($data))
		{
			return $data;
		}else{
			return $output;
		}
	}
}
