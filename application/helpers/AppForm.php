<?php

namespace App\Helpers;

/**
 * 
 * Static form helper
 * 
 */
class AppForm{
    
    // example {!! App\Helpers\AppForm::input('text', 'email','email') !!}
    static function input($type="text", $label="label", $name="name", $required=false, $value="",$placeholder=null, $attr="", $footnote="", $class=""){
        $required=$required ? "required":"";
        $placeholder=$placeholder ? $placeholder:ucfirst($label);
        $pattern = $type == "password"?"pattern=\".{8,}\"":"";
        $checked = "";

        if($type == "checkbox"){
            $checked = $value ? "checked":"";
        }

        return "<div class=\"form-group row\">
            <label for=\"$name\" class=\"col-sm-3 col-form-label\">".ucwords($label)."</label>
            <div class=\"col-sm-9\">
                <input type=\"$type\" $checked 
                class=\"form-control $class\" id=\"$name\" 
                name=\"$name\" placeholder=\"".$placeholder."\" $required value=\"$value\" $pattern $attr>
            </div>
            <div class=\"invalid-feedback\">
                $footnote
            </div>
        </div>";
    }
    
    static function select($label="label", $name="name", $data=[],$required=false, $value="", $initoption="-", 
    $multiple=null){
        $required=$required ? "required":"";
        $multiple = $multiple ? 'multiple="multiple"':"";
        $opts="";

        $id = preg_replace('/\[\]/','', $name);

        foreach($data as $d){
            $select = "";
            if($value == $d){
                $select = "selected";
            }
            $opts.="<option $select value=\"$d\">".ucwords($d)."</option>";
        }

        return "<div class=\"form-group row\">
            <label for=\"$name\" class=\"col-sm-3 col-form-label\">".ucwords($label)."</label>
            <div class=\"col-sm-9\">
                <select class=\"form-control\" name=\"$name\" id=\"$id\" $multiple>
                    <option>$initoption</option>
                    $opts
                </select>
            </div>
        </div>";
    }

    static function selectNamed($label="label", $name="name", $data=[[]],$required=false, $value=""){
        $required=$required ? "required":"";
        $opts="";
        foreach($data as $d){
            $select = "";
            if($value == $d[0]){
                $select = "selected";
            }
            $opts.="<option $select value=\"$d[0]\">".ucwords($d[1])."</option>";
        }

        return "<div class=\"form-group row\">
            <label for=\"$name\" class=\"col-sm-3 col-form-label\">$label</label>
            <div class=\"col-sm-9\">
                <select class=\"form-control\" name=\"$name\" id=\"$name\">
                    <option>-</option>
                    $opts
                </select>
            </div>
        </div>";
    }

    static function selectModel($label="label", $name="name", $data=[], $col1, $col2,$required=false, $value=""){
        $required=$required ? "required":"";
        $opts="";
        foreach($data as $d){
            $select = "";
            if($value == $d->$col1){
                $select = "selected";
            }
            $opts.="<option $select value=\"".$d->$col1."\">".ucwords($d->$col2)."</option>";
        }

        return "<div class=\"form-group row\">
            <label for=\"$name\" class=\"col-sm-3 col-form-label\">$label</label>
            <div class=\"col-sm-9\">
                <select class=\"form-control\" name=\"$name\" id=\"$name\">
                    <option>-</option>
                    $opts
                </select>
            </div>
        </div>";
    }

    static function textarea($label="Textarea", $name, $value, $placeholder=null){
        $label = ucwords($label);
        $placeholder = $placeholder ? $placeholder : ucfirst($label);

        return "<div class=\"form-group\">
            <label for=\"$name\">$label</label>
            <textarea class=\"form-control form-control-cust\" placeholder=\"$placeholder\" required=\"\" name=\"$name\" style=\"resize: vertical;\">$value</textarea>
        </div>";
    }
}