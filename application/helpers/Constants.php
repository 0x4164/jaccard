<?php

class Constants{
    const DASHBOARD_FILTER_YEAR = "tahun";
    const REST_MSG_SUCCESS = "Sukses";
    
    static function years($start = 2019, $end = 2022){
        $y = [];
        $n = abs(abs($end) - abs($start));
        $i = 0;
        $y[]=$start;

        while($i < $n){
            $y[] = ++$start;
            $i++;
        }

        return $y;
    }

    static function arrToOpt($arr){
        $opt = "";
        foreach($arr as $a){
            $opt .= "<option value=\"$a\">$a</option>";
        }
        return $opt;
    }

    // randString(4)
    static function randString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
    {
        $str = '';
        $count = strlen($charset);
        while ($length--) {
            $str .= $charset[mt_rand(0, $count-1)];
        }
        return $str;
    }


    static function randStr($str){
        $ret = $str."-".self::randString(5);
        
        return $ret;
    }

    static function validDateOrNow($str){
        return strtotime($str) ? $str
             : date('Y-m-d H:i:s');;
    }

    static function anchor($str, $href){
        return "<a style=\"color:white\" href=\"$href\">$str</a>";
    }
    static function page($bigTitle = "Hasil", $trsFun = null){
        if($trsFun == null){
            $trsFun = function(){};
        }
        ?>
        <main>
            <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
                <div class="container">
                    <div class="page-header-content pt-4">
                        <div class="row align-items-center justify-content-between">
                            <div class="col-auto mt-4">
                                <h1 class="page-header-title">
                                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                                    <?= $bigTitle ?>
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- Main page content-->
            <div class="container mt-n10">
                <div class="card mb-4">
                    <div class="card-header">Semua Data
                        <!-- <a class="btn btn-success" href="<?= base_url('admin/kuisioner/create') ?>">
                            Tambah data
                        </a> -->
                    </div>
                    <div class="card-body">
                        <?php $trsFun(); ?>
                    </div>
                </div>
            </div>
        </main>
        <?php
    }

    static function topmenu($title = "title", $href = "#"){
        ?>
        <a class="dropdown-item py-3" href="<?= $href ?>" target="_blank">
            <div class="icon-stack bg-primary-soft text-primary mr-4"><i data-feather="book"></i></div>
            <div>
                <div class="small text-gray-500"><?= $title ?></div>
                <!-- Usage instructions and reference -->
                <?= $title ?>
            </div>
        </a>
        <?php
    }
}