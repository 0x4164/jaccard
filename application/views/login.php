<header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
	<div class="container">
		<div class="page-header-content pt-4">
			<div class="row align-items-center justify-content-between">
				<div class="col-auto mt-4">
					<h1 class="page-header-title">
						<div class="page-header-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layout"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="3" y1="9" x2="21" y2="9"></line><line x1="9" y1="21" x2="9" y2="9"></line></svg></div>
						Login
					</h1>
					<div class="page-header-subtitle">The custom page header supports and styles Bootstrap breadcrumbs</div>
				</div>
			</div>
		</div>
	</div>
</header>
<div class="container mt-n10">
<div class="card">
	<div class="card-body p-0">
		<div class="row no-gutters">
			<!-- <div class="col-lg-6 align-self-stretch bg-img-cover d-none d-lg-flex" style='background-image: url("https://source.unsplash.com/npxXWgQ33ZQ/1200x800")'></div> -->
			<div class="col-lg-6 p-5 col-lg-offset-2">
				<h3>Login aplikasi</h3>
				<form action="<?= base_url('auth/login/submit') ?>" target="" method="post">
					<p><?php echo validation_errors();?></p>
					<input class="form-control mb-2" name="username" type="email" placeholder="Masukkan Email">
					<input class="form-control mb-2" name="password" type="password" placeholder="Masukkan Password">
					<button type="submit" class="btn btn-primary">Login</button>
				</form>
				<hr>
				<h3>Belum memiliki akun?</h3>
				<a href="<?php echo site_url('page/registrasi'); ?>">Klik Disini untuk Registrasi</a>
			</div>
		</div>
	</div>
</div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url()?>js/scripts-admin.js"></script>

