<script>
    var baseUrl = "<?= base_url() ?>";
    var urls = {
        edit : baseUrl+"admin/diklat/edit/data-id",
        diklat : {
            peserta: baseUrl+"admin/diklat/edit/data-id/peserta",
            kuisioner: baseUrl+"admin/diklat/edit/data-id/kuisioner",
        },
        filter: baseUrl+"admin/dashboard/filter",
        datatable:{
            all: baseUrl+"datatables/proses/0/ctx-sim"
        }
    }

    var Fields = {
        tahun: $('#tahun'),
        values: {
            diklatCnt: $('#diklatCnt'),
            pesertaCnt: $('#pesertaCnt'),
        }
    }

    var queries = {};
    var dtElement = $('#dataTable');


	// $(document).ready(function() {
    dtElement.DataTable(
        datatablesAjaxConf(urls.datatable.all, [
            AppDatatable.util.colNumbering('id','id'),
            { data: 'cnama', name: 'cnama' },
            { data: 'c2nama', name: 'c2nama' },
            { data: 'spnama', name: 'spnama' },
            { data: 'sp2nama', name: 'sp2nama' },
            { data: 'ctxsimcalc', name: 'ctxsimcalc' },
            { data: function(data){
                // var id = data.id;
                // id = "#";
                var btns = "";
                // btns+= Components.a.btn.user.replace('[href]',
                //     urls.diklat.peserta.replace('data-id',id)
                // )
                // btns+= Components.a.btn.edit.replace('[href]',
                //     urls.diklat.kuisioner.replace('data-id',id)
                // )
                // btns+= Components.btn.delete.replace('[onclick]',"deleteData("+id+")")
                return btns;
            }, name: 'name', orderable: false, searchable: false },
        ], false)
    );
    // });


    dtElement.on('search', function () {
        // alert('Table redrawn');
        console.log(urls.datatable.all);
        this.ajax.url(urls.datatable.all)
    });

    Fields.tahun.on('change', function(){
        let tahun = Fields.tahun.val();
        queries.tahun = tahun;
        // filter()
    })

    function filter(by = "tahun"){
        var srcParam = {
            by: by,
        }
        Object.assign(srcParam, queries); // merge objs
        var paramStr = Helpers.objToQueryStr(srcParam);
        // console.log(srcParam)
        var urlAjax = urls.filter +"?"+ paramStr;

        reqAjax(urlAjax, "get", {}, function(respond){
            var data = respond.data;
            Fields.values.pesertaCnt.text(data.count.peserta)
            Fields.values.diklatCnt.text(data.count.diklat)
        })
        
        if(queries.tahun && !queries.init){
            var newUrl = urls.datatable.all+"?"+paramStr
            console.log(newUrl)
            console.log(dtElement)
            dtElement.DataTable().ajax.url(newUrl).load()
            queries.init = false

            // DataTable()
        }
        
        queries={};
    }

    function init(){
        queries.tahun = 2020;
        queries.init = true;
        // filter("tahun")
    }

    init()
</script>

