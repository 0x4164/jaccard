<script>
    var useGroup = <?= $type === "rpa-group" ? "true":"false" ?>;
    var baseUrl = "<?= base_url() ?>";
    var urls = {
        edit : baseUrl+"admin/diklat/edit/data-id",
        diklat : {
            peserta: baseUrl+"admin/diklat/edit/data-id/peserta",
            kuisioner: baseUrl+"admin/diklat/edit/data-id/kuisioner",
        },
        filter: baseUrl+"admin/dashboard/filter",
        datatable:{
            all: baseUrl+"datatables/proses/0/rpa",
            group: baseUrl+"datatables/proses/0/rpa?rpa-group=1",
        }
    }

    var Fields = {
        tahun: $('#tahun'),
        values: {
            diklatCnt: $('#diklatCnt'),
            pesertaCnt: $('#pesertaCnt'),
        }
    }

    var queries = {};
    var dtElement = $('#dataTable');


	// $(document).ready(function() {
    var useUrl = useGroup ? urls.datatable.group: 
        urls.datatable.all ;

    dtElement.DataTable(
        datatablesAjaxConf(useUrl, [
            AppDatatable.util.colNumbering('id','id'),
            { data: 'cnama', name: 'cnama' },
            { data: 'c2nama', name: 'c2nama' },
            { data: 'sim_overall', name: 'sim_overall' },
            { data: 'cluster', name: 'cluster' },
            { data: 'acc', name: 'acc' },
            { data: 'cnt', name: 'cnt' },
            { data: function(data){
                // var id = data.id;
                // id = "#";
                var btns = "";
                // btns+= Components.a.btn.user.replace('[href]',
                //     urls.diklat.peserta.replace('data-id',id)
                // )
                // btns+= Components.a.btn.edit.replace('[href]',
                //     urls.diklat.kuisioner.replace('data-id',id)
                // )
                // btns+= Components.btn.delete.replace('[onclick]',"deleteData("+id+")")
                return btns;
            }, name: 'name', orderable: false, searchable: false },
        ], false)
    );
    // });


    dtElement.on('search', function () {
        // alert('Table redrawn');
        console.log(urls.datatable.all);
        this.ajax.url(urls.datatable.all)
    });

    Fields.tahun.on('change', function(){
        let tahun = Fields.tahun.val();
        queries.tahun = tahun;
        // filter()
    })

    function filter(by = "tahun"){
        var srcParam = {
            by: by,
        }
        Object.assign(srcParam, queries); // merge objs
        var paramStr = Helpers.objToQueryStr(srcParam);
        // console.log(srcParam)
        var urlAjax = urls.filter +"?"+ paramStr;

        reqAjax(urlAjax, "get", {}, function(respond){
            var data = respond.data;
            Fields.values.pesertaCnt.text(data.count.peserta)
            Fields.values.diklatCnt.text(data.count.diklat)
        })
        
        if(queries.tahun && !queries.init){
            var newUrl = urls.datatable.all+"?"+paramStr
            console.log(newUrl)
            console.log(dtElement)
            dtElement.DataTable().ajax.url(newUrl).load()
            queries.init = false

            // DataTable()
        }
        
        queries={};
    }

    function init(){
        queries.tahun = 2020;
        queries.init = true;
        // filter("tahun")
    }

    init()
</script>

