<?php
// include_once(__DIR__.'/../../helpers/AppForm.php');
?>
<main>
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
    <?php
        include_once(__DIR__.'/../../../../helpers/AppForm.php');
    ?>
        <div class="container">
            <div class="page-header-content pt-4">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto mt-4">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="activity"></i></div>
                            Edit Kuisioner
                        </h1>
                        <div class="page-header-subtitle">Example dashboard overview and content summary</div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container mt-n10">
        <div class="card mb-4">
            <div class="card-header">Edit Kuisioner</div>
            <div class="card-body">
            <form action="<?= base_url('admin/kuisioner/update/submit')?>" method="post">
                    <?php
                    $fields = $this->kuisioner_model->fillable;
                    $slice = [
                        array_slice($fields, 0, sizeof($fields)/2),
                        array_slice($fields, sizeof($fields)/2)
                    ];
                    
                    $dataArr = (array) $data;
                    ?>
                    <div class="row">
                        <div class="col-lg-6">
                        <input type="hidden" name="id" id="" value="<?= $data->id ?>">
                        <?php
                        foreach($slice[0] as $f){
                            echo App\Helpers\AppForm
                                ::input('text', $f, $f, true, $dataArr[$f]);
                        }
                        ?>
                        </div>
                        <div class="col-lg-6">
                        <?php
                        foreach($slice[1] as $f){
                            echo App\Helpers\AppForm
                                ::input('text', $f, $f, true, $dataArr[$f]);
                        } ?>
                        </div>
                    </div>
                    <input class="form-control" type="submit">
                </form>
            </div>
        </div>
    </div>
</main>