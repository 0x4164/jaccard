<?php
// include_once(__DIR__.'/../../helpers/AppForm.php');
?>
<main>
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
    <?php
        include_once(__DIR__.'/../../../../helpers/AppForm.php');
    ?>
        <div class="container">
            <div class="page-header-content pt-4">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto mt-4">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="activity"></i></div>
                            Tambah Kuisioner
                        </h1>
                        <div class="page-header-subtitle">Example dashboard overview and content summary</div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container mt-n10">
        <div class="card mb-4">
            <div class="card-header">Tambah Kuisioner</div>
            <div class="card-body">
                <form action="<?= base_url('admin/kuisioner/create/submit')?>" method="post">
                    <?php
                    $fields = $this->kuisioner_model->fillable;
                    $slice = [
                        array_slice($fields, 0, sizeof($fields)/2),
                        array_slice($fields, sizeof($fields)/2)
                    ];
                    $empty = "1";
                    
                    ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Info kuisioner</h2>
                        </div>
                        <div class="col-lg-12">
                        <?php
                        $dt = date("Y-m-d H:i:s");
                        ?>
                        <?= App\Helpers\AppForm::input('text', "id_diklat", "kinfo[id_diklat]", true, 8) ?>
                        <?= App\Helpers\AppForm::input('text', "name", "kinfo[name]", true, $empty) ?>
                        <?= App\Helpers\AppForm::input('text', "desc_short", "kinfo[desc_short]", true, $empty) ?>
                        <?= App\Helpers\AppForm::input('text', "desc_long", "kinfo[desc_long]", true, $empty) ?>
                        <?= App\Helpers\AppForm::input('text', "by", "kinfo[by]", true, $empty) ?>
                        <?= App\Helpers\AppForm::input('text', "opened_at", "kinfo[opened_at]", true, $dt) ?>
                        <?= App\Helpers\AppForm::input('text', "end_at", "kinfo[end_at]", true, $dt) ?> 
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-12">
                            <button class="btn btn-success" type="button">Lihat template</button>
                            <button class="btn btn-success" type="button" onclick="act(1)">Tambah pertanyaan</button>
                            <a target="_blank" class="btn btn-success" href="<?= base_url('public/kuisioner/answer/uniq1') ?>">Coba jawab</a>
                            <!-- <button class="btn btn-success" type="button" onclick="act(1)">Coba jawab</button> -->
                        </div>
                        <div id="questionFields" class="col-lg-12">
                        </div>
                    </div>
                    <div id="surveyElement" style="display:inline-block;width:100%;"></div>
                    <div id="surveyResult"></div>
                    <input class="form-control" type="submit">
                </form>
            </div>
        </div>
    </div>
</main>