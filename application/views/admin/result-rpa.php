<?php
// include_once(__DIR__.'/../../helpers/AppForm.php');
include_once(__DIR__.'/../../helpers/Constants.php');

// include_once(__DIR__.'/../../helpers/AppForm.php');
// include_once(__DIR__.'/../../../helpers/Constants.php');

$group = $type === "rpa-group" ;
$anchor = $group? 
    Constants::anchor("Data RPA Group", base_url("home/result/rpa"))
    : Constants::anchor("Data RPA", base_url("home/result/rpa-group"));

Constants::page(
    $anchor
    , function() use ($group, $rpa) {
?>
<div class="datatable">
    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>cnama</th>
                <th>cnama2</th>
                <th>sim_overall</th>
                <th>cluster</th>
                <th>acc</th>
                <th>cnt</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<?php
if($group){
?>
<hr>
<div class="row">
    <div class="col-lg-12">
        <h2>RPA</h2>
    </div>
    <div class="col-lg-12">
        <table class="table table-bordered">
        <?php
        // border="5px" 
            foreach($rpa as $key => $rv){
                ?>
                <tr> 
                    <td> <?= ucfirst($key) ?></td>
                    <td> <?= sprintf("%0.3f", $rv*100) ?> %</td>
                </tr>
                <?php
            }
        ?>
        </table>
    </div>
</div>
<?php
}
});
?>