<?php
// include_once(__DIR__.'/../../helpers/AppForm.php');
include_once(__DIR__.'/../../helpers/Constants.php');
?>
<main>
	<header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
		<div class="container">
			<div class="page-header-content pt-4">
				<div class="row align-items-center justify-content-between">
					<div class="col-auto mt-4">
						<h1 class="page-header-title">
							<div class="page-header-icon">
								<i data-feather="activity"></i>
							</div>
                            Dashboard
						</h1>
						<div class="page-header-subtitle">Example dashboard overview and content summary</div>
					</div>
					<div class="col-12 col-xl-auto mt-4">
						<select name="" id="tahun" class="form-control">
							<option value="">-- Pilih tahun --</option>
							<?= Constants::arrToOpt(Constants::years()) ?>
							<!-- <option value="">2020</option> -->
						</select>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!-- Main page content-->
	<div class="container mt-n10">
		<div class="row">
			<div class="col-xxl-3 col-lg-6">
				<div class="card bg-success text-white mb-4">
					<div class="card-body">
						<div class="d-flex justify-content-between align-items-center">
							<div class="mr-3">
								<div class="text-white-75 small">Jumlah Diklat</div>
								<div id="diklatCnt" class="text-lg font-weight-bold">0</div>
							</div>
							<i class="feather-xl text-white-50" data-feather="calendar"></i>
						</div>
					</div>
					<div class="card-footer d-flex align-items-center justify-content-between">
						<a class="small text-white stretched-link" href="#">View Report</a>
						<div class="small text-white">
							<i class="fas fa-angle-right"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xxl-3 col-lg-6">
				<div class="card bg-warning text-white mb-4">
					<div class="card-body">
						<div class="d-flex justify-content-between align-items-center">
							<div class="mr-3">
								<div class="text-white-75 small">Jumlah Peserta</div>
								<div id="pesertaCnt" class="text-lg font-weight-bold">0</div>
							</div>
							<i class="feather-xl text-white-50" data-feather="dollar-sign"></i>
						</div>
					</div>
					<div class="card-footer d-flex align-items-center justify-content-between">
						<a class="small text-white stretched-link" href="#">View Report</a>
						<div class="small text-white">
							<i class="fas fa-angle-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Example Colored Cards for Dashboard Demo-->
		<!-- Example Charts for Dashboard Demo-->
		<div class="card mb-4">
			<div class="card-header">Data Diklat</div>
			<div class="card-body">
				<div class="datatable">
					<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>No</th>
								<th>Jenis Diklat</th>
								<th>Nama Diklat</th>
								<th>Alias</th>
								<th>Mulai</th>
								<th>Jumlah Peserta</th>
								<th>Jumlah JPL</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</main>