<?php
include_once('Dataset.php');

class ChartData{
    public $labels,
        $datasets;

    public function __construct($labels = [], $datasets = []){
        $this->labels = $labels;
        $this->datasets = $datasets;
    }

    public function setLabels($labels){
        $this->labels = $labels;

        return $this;
    }

    public function addLabel($label = "data"){
        $this->labels[] = $label;

        return $this;
    }

    public function addDatasets($datasets = null){
        if($datasets instanceof Dataset){
            $this->datasets[] = $dataset;
        }

        return $this;
    }

    public function datasetsFromArr($datasets = []){
        $this->datasets = $datasets;

        return $this;
    }

    public function getFormatted($datasets = []){
        foreach($this->datasets as $datasets){

        }

        return $this;
    }

    public function toJson(){
        return json_encode($this);
    }
}