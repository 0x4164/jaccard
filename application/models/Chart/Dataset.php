<?php

class Dataset{
    public $label = "Label",
        $backgroundColor = "rgba(0, 97, 242, 1)",
        $borderColor = "#4e73df",
        $hoverBackgroundColor = "rgba(0, 97, 242, 0.9)",
        $data = 3;

    public function __construct($label="Label", 
        $backgroundColor=null, $borderColor=null, $hoverBackgroundColor="", $data=1){
        $this->label = $label;
        $this->backgroundColor = $backgroundColor;
        $this->borderColor = $borderColor;
        $this->hoverBackgroundColor = $hoverBackgroundColor;
        $this->data = $data;
    }

    public function setDefaultColor(){
        $this->backgroundColor = "rgba(0, 97, 242, 1)";
        $this->borderColor = "rgba(0, 97, 242, 0.9)";
        $this->hoverBackgroundColor = "#4e73df";
        
        return $this;
    }

    public function setLabel($label){
        $this->label = $label;

        return $this;
    }

    public function setData($data){
        $this->data = $data;

        return $this;
    }
}