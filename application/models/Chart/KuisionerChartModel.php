<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__.'/ChartData.php');
require_once(__DIR__.'/Dataset.php');

class KuisionerChartModel extends CI_Model {

	public function chart($kuesionerid){
		$allResponden = $this->getRespondenBy('kuesionerid', $kuesionerid);
		// lq();
		// preout($allResponden);
		// array of chartdata objs structure
		$cdArr = [];

		foreach($allResponden as $ar){
			// preout($ar);
			$cd = new ChartData();

			$wheres = [
				'choice.question_id' => $ar->qid
			];

			$qRet = $this->getRespondenBy('wheres', null, $wheres);
			$dataArr = [];
			foreach($qRet as $r){
				// preson($r->question_id." "." ".$r->val." :: ".$r->name." ".$r->content);
				$dataArr[] = $r->val;
				$cd->addLabel($r->label);
			}
			
			$data = [];
			$ds = new Dataset();
			$ds->setDefaultColor()
				->setData($dataArr);
			$data[] = $ds;
			$datasets = $data;
			$cd->datasetsFromArr($datasets);
			
			$cdArr["chartq".$ar->qid] = $cd;
		}

		return $cdArr;
	}

	public function baseQuery($type = null){
		
		$builder = $this->db->select('*, ques.id as qid, 
				ques.name as qname, ques.content as qcontent, 
				choice.content as label, count(resp.id) as val')
			->from('mdl_questionnaire a')
			->join("mdl_questionnaire_response resp", "a.id = resp.questionnaireid")
			->join("mdl_questionnaire_resp_single sing", "sing.response_id = resp.id")
			->join("mdl_questionnaire_question ques", "ques.id = sing.question_id")
			->join("mdl_questionnaire_quest_choice choice", "choice.id = sing.choice_id");
		$builder->group_by("ques.id");

		switch($type){
			case "choice":
				$builder->group_by("choice.content");
			break;
			default:

		}
		
		return $builder;
	}

	public function getDataset($dataArr){
		$datasets = [];
		$n = 0;

		$ds = new Dataset("");
		$ds->setDefaultColor()
			->setData($data);
		$datasets[] = $ds;

		return $datasets;
	}

	public function getRespondenBy($by = null, $id = null, $wheres = null){
		// $id = $this->db->escape($id);
		$builder = $this->baseQuery("choice");
		
		switch($by){
			case 1:
			break;
			case "wheres":
				$builder->where($wheres);
			break;
			case "kuesionerid":
			default:
				// questionnaireid
				$builder->where("a.id", $id);
		}
		
		$r = $builder->get()->result();

		return $r;
	}

	public function labelAlternate($val){
		$alt = "";
		
		if($val < 70){
			$alt = "Tidak Baik / Responsif";
		}else if($val >= 70 && $val < 77.49){
			$alt = "Cukup Baik / Responsif";
		}else if($val >= 75 && $val <= 79.99){
			$alt = "Baik Baik / Responsif";
		}else if($val >= 80 && $val <= 89.99){
			$alt = "Kurang Baik / Responsif";
		}else if($val >= 90 && $val <= 95){
			$alt = "Sangat Baik / Responsif";
		}

		return $alt;
	}
}