<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__.'/Base/BaseModel.php');

class ProsesModel extends BaseModel {
	public $table = "proses";

	public function countComponent(){
		$q = "SELECT p.*, p.cnt_task + p.cnt_branch + 
			p.cnt_connector as cnt_all FROM proses p";
		return $this->db->query($q)->result();
	}

	public function all(){
		$q = "SELECT p1.*, c.nama, sp.* FROM proses p1
			join commerce c on p1.idcommerce = c.id
			join subproses sp on sp.idproses = p1.id
			where sp.type = 'task'
			order by c.nama asc";

		return $this->db->query($q)->get();
	}
}