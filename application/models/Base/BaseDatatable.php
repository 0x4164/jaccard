<?php
// datatable
abstract class BaseDatatable extends CI_Model{
    protected $table = "table";
	protected $tableView = "table";
	
	protected $select = '*';
	protected $column_search = array('id');

	protected $searchBy = [ "firstname" ];
	
	protected $column_order = array(null, 'id', null);
	protected $order = array('id' => 'desc');
	protected $query = "";

	private $dbQuery;

    public function __construct(){
        
	}
	
	// @return array
	abstract public function getCustomSearchValue();
	
    public function getSearchValue(){
		$ret = arrayGet($_GET, 'search');
		$val = arrayGet($ret, 'value');
		
        return $val;
	}

    public function hasCustomSearchValue(){
        return sizeof($this->getCustomSearchValue()) > 0;
	}

    public function hasSearchValue(){
        return strlen($this->getSearchValue()) > 0;
	}
	
	// override this
    public function search(){
        return [];
	}
	
    public function setLimit($q){
		$l1 = (int) arrayGet($_GET, "start");
		$l2 = (int) arrayGet($_GET, "length");
		return $q." limit $l1, $l2";
    }

    function get($query = null, $view = null)
	{
		
		$return = new stdClass();

		$queryTotal = $this->db->query($query);

		$limitQuery = $query;
		$limitQuery = $this->setLimit($query);
		$query = $this->db->query($limitQuery);

		if($this->hasSearchValue() || $this->hasCustomSearchValue()){
			// $query = $this->search();
		}
		
		$return->data = $query->result();
		$return->recordsTotal = $queryTotal->num_rows();
		$return->recordsFiltered = $queryTotal->num_rows();
		return $return;
	}

	protected function _get($view = null)
	{
        $select = $this->select;
        
		$column_search = $this->column_search;
		$column_order = ['id'];
		$order = $this->order;

		if ($select) {
			// $this->db->select($select);
		}

        $this->_datatable_search($column_search, $column_order, $order);
	}

	function _datatable_output($data, $recordsTotal, $recordsFiltered)
	{
		$output = [
			'draw' => $this->input->get('draw'),
			'recordsTotal' => $recordsTotal,
			'recordsFiltered' => $recordsFiltered,
			'data' => $data,
		];
		return jsonResponse(200, "", $output, false);
	}

	public function getQuery(){
		$builder = $this->db->select('select 1,2,3,4');
		
		$ret = $builder->get_compiled_select();

        return $ret;
	}

	/*
		get rendered datatable
		implement this
	*/
	abstract function getRendered();
}