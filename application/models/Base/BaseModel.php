<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

class BaseModel extends CI_model {
	protected $posts;
	protected $gets;
	public $table;

	function __construct()
	{
		parent:: __construct();
	}

	public function posts(){
		return $this->input->post();
	}

	public function gets(){
		return $this->input->get();
	}

	public function single($tbl, $wheres){
		return $this->db->select('*')->from($tbl)
			->where($wheres)
			->get()->row();
	}

	public function singleData($wheres){
		return $this->db->select('*')->from($this->table)
			->where($wheres)
			->get()->row();
	}

	public function getMulti($wheres){
		return $this->db->select('*')->from($this->table)
			->where($wheres)
			->get()->result();
	}

	public function create($data){
		$this->db->insert($this->table, $data);
		return $data;
	}

	public function update($data, $where){
		$this->db
			->where($where)
			->update($this->table, $data);
		return $data;
	}

	public function delete($where){
		$this->db
			->where($where)
			->delete($this->table);
		return $data;
	}

	public function pluck($arr, $col){
		return array_column($arr, $col) ;
	}
}