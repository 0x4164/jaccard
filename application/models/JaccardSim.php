<?php

class JaccardSim extends CI_Model{
    public $nmax = 0, $vmax = 0, $amax = 0;
	public $contextualSimArr = [];
	
	function __construct()
	{
		parent:: __construct();
		// $this->current_db = $this->load->database('default',TRUE);
	}

	public function getNmax(){
		return $this->nmax;
	}

	public function setNmax($nmax){
		$this->nmax = $nmax;

		return $this;
	}
 
	public function getVmax(){
		return $this->vmax;
	}

	public function setVmax($vmax){
		$this->vmax = $vmax;

		return $this;
	}
 
	public function getAmax(){
		return $this->amax;
	}

	public function setAmax($amax){
		$this->amax = $amax;

		return $this;
	}

	public function getMaxByType($type){
		$max = 0;
		switch($type){
			case "noun":
				$max = $this->getNmax();
			break;
			case "verb":
				$max = $this->getVmax();
			break;
			case "adjective":
				$max = $this->getAmax();
			break;
		}

		return $max;
	}

	public function setMaxByType($type, $max){
		switch($type){
			case "noun":
				$this->setNmax($max);
			break;
			case "verb":
				$this->setVmax($max);
			break;
			case "adjective":
				$this->setAmax($max);
			break;
		}

		return $this;
	}

	public function getContextualSimArr(){
		return $this->contextualSimArr;
	}

	public function setContextualSimArr($contextualSimArr){
		$this->contextualSimArr = $contextualSimArr;

		return $this;
	}
	
	public function import(){
        $arrDb = ['noreg']; // selct
        $arrX = ['c1','c2','noreg'];

        foreach($arrX as $ax){

            if( in_array($ax->noreg, $arrDb) ){
                //dup
            }else{
                //no dup
            }
        }
	}

	public function word($w = "a", $cat = ""){
		// get word category
		// $category = ["noun", "verb"];
		$category = ["pendaftaran" => "noun",
			"mengisi" => "verb",
			"identitas" => "noun",
			"aktivasi" => "noun",
			"nomor" => "noun",
			"ponsel" => "noun",
			"kirim" => "verb",
			"ulang" => "noun",
			"mendaftarkan" => "verb",
			"memeriksa" => "verb",
			"toko" => "noun",
		];
		// $cat = $category[array_rand($category, 1)];
		$cat = $category[$w];
		/*
		*/
		return [
			"word" => $w,
			"type" => $cat,
		];
	}
/*
if($noun){
			$maxTaxonomyPath = 5;
			$shortPath = 3;
		}else{
			$maxTaxonomyPath = 4;
			$shortPath = 2;
		}
*/
	// public function pathSimmilarity($c1 = "c1", $c2 = "c2", $noun = true){
		// , $noun = true
	public function pathSimmilarity($maxTaxonomyPath = 1, $shortPath = 1){
		// preout(__METHOD__);
		// max of legit node visited
		
		// if not exist @db then crawl / scrap
		/*
		get from db, 
		
		if maxTaxonomyPath == 0 || $shortPath == 0 
			then count with crawl / scrap
		else
		
		$maxTaxonomyPath = 3;
		$shortPath = 1;
		*/
		$pathLength = 2 * $maxTaxonomyPath - $shortPath;
		// preout("pathLength ".$pathLength);

		// + random_int(1,10)
		$ret = 1 / $pathLength;
		return $ret;
	}

	public function contextualSimmilarity($nmax = 0, $vmax = 0, $amax = 0){
		$sim = 0;
		$wN = 0.25;
		$wV = 0.75;
		$aV = 0.75;

		if($nmax > 0){
			$sim += $nmax * $wN;
		}
		if($vmax > 0){
			$sim += $vmax * $wV;
		}
		if($amax > 0){
			$sim += $amax * $wA;
		}
		// + random_int(1,10)/10
		return $sim;
	}

	public function countThreshold($arr = [], $threshold = 0){
		$cnt = 0;
		foreach($arr as $a){
			if((float) $a >= $threshold){
				$cnt++;
			}
		}

		return $cnt;
	}

	public function getSinonim($w1 = "a", $w2 = "a", $noun = true){
		// preout(__METHOD__);
		// get syn
		// $pathLength = 0;
		// $simVal = random_int(1,100)/100;
		$v1 = 5;
		$v2 = 3;
		$simVal = $this->pathSimmilarity($v1, $v2, $noun);
		return [
			"sinonim" => "word",
			"simmilarityVal" => sprintf("%0.3f", $simVal),
		];
	}

	public function parseXML($fname = ""){
		preout("file ".$fname);
		$xml = simplexml_load_file($fname);
		// $get = json_decode($xml);
		// $get = $get;
		// preson($xml);
		// preout($xml->Pools);
		$attr = "@attributes";
		// preout($xml->WorkflowProcesses);
		// preout($xml->WorkflowProcesses->WorkflowProcess);
		foreach($xml->WorkflowProcesses->WorkflowProcess as $wproc){
			preout((string) $wproc['Name']);
			// preout($wproc->Activities);
			// ->Activity

			if($wproc->Activities){
				$acts = sizeof($wproc->Activities);
				// preout($acts);
				if($acts > 0){
					// preout($wproc->Activities->Activity[0]['Name']);
					// preout($wproc->Activities->Activity[1]);
					// preout($wproc->Activities->Activity[2]);
					
					foreach($wproc->Activities->Activity as $act){
						preout((string) $act['Name']);
					}
				}
			}

			// preout($wproc);
		}
		preout("");
	}

	public function parseData(){
		// $filename = "dummy/TOKOPEDIA.xpdl";
		// $f = fopen($filename,"r");
		// $xml_string = fread($f, filesize($filename));
		// preout($xml_string);
		// exit;
		// echo $xml_string;
		// exit;
		// $xml = simplexml_load_string($xml_string);
		$fname = "dummy/input brg tokopedia.xpdl";
		$fname = "dummy/TOKOPEDIA.xpdl";
		$this->parseXML($fname);

		$fname = "dummy/input brg shopee.xpdl";
		$this->parseXML($fname);
		
		// preson($xml);
		// preson($get);
	}

	// path algo
	public function semanticSimmilarity(){
		preout(__METHOD__);
		preout("Path method");
		preout("Tokped 
		Structural : startA A AB B BR1 R1 R1C C CB R1D D Dend
		Semantic : Pendaftaran mengisi identitas - Aktivasi nomor ponsel - Kirim ulang - Mendaftarkan toko");
		preout("Bukalapak
		Structural : startA A AB B BR1 R1 R1C C CB R1end
		Semantic : Pendaftaran mengisi identitas - Memeriksa nomor ponsel - Kirim ulang");

		$t1Str = "Pendaftaran mengisi identitas - Aktivasi nomor ponsel - Kirim ulang - Mendaftarkan toko";
		$t2Str = "Pendaftaran mengisi identitas - Memeriksa nomor ponsel - Kirim ulang";

		$t1Str = strtolower($t1Str);
		$t2Str = strtolower($t2Str);

		preout($t1Str);
		preout($t2Str);

		$sentc1 = explode(" - ",$t1Str);
		$sentc2 = explode(" - ",$t2Str);

		preout($sentc1);
		preout($sentc2);
		
		$matrix = [];
		$threshold = 0.25;
		$contextualSimArr = [];
		// x ∩ y
		foreach($sentc1 as $key => $s1){
			$el = [];
			$words1 = explode(" ",$s1);
			// $nmax = 0; $vmax = 0; $amax = 0;

			foreach($sentc2 as $key => $s2){
				//select db
				$wArr = [];
				$words2 = explode(" ",$s2);
				foreach($words1 as $key => $w1){
					foreach($words2 as $key => $w2){
						// $sinonim = false ? "data" : "-";
						$wo1 = $this->word($w1);
						$wo2 = $this->word($w2);

						$sameType = $wo1["type"] === $wo2["type"];
						if($sameType){
							$sinonim = $this->getSinonim($w1, $w2, $wo1["type"] === "noun");

							$getMax = $this->getMaxByType($wo1["type"]);
							if($getMax < (double) $sinonim['simmilarityVal']){
								$this->setMaxByType($wo1["type"], $sinonim['simmilarityVal']);
							}
						}else{
							$sinonim = null;
						}

						$wArrE = [
							"compare" => $w1." : ".$w2,
							"w1" => $wo1,
							"w2" => $wo2,
							"sinonim" => $sinonim,
						];
						$wArr[] = $wArrE;
					}
				}

				$contextualSim = $this->contextualSimmilarity(
					$this->getMaxByType("noun"),
					$this->getMaxByType("verb"),
					$this->getMaxByType("adjective")
				);

				$contextualSimArr[] = [
					"sentences" => $s1.":".$s2,
					"val" => $contextualSim
				];

				$el[] = [
					"k1" => $s1,
					"k2" => $s2,
					"words" => $wArr,
					"max" => [
						"noun" => $this->getMaxByType("noun"),
						"verb" => $this->getMaxByType("verb"),
						"adjective" => $this->getMaxByType("adjective")
					],
					"contextualSim" => $contextualSim,
				];
			}
			
			$matrix[] = $el;
		}

		$cntThres = $this->countThreshold($contextualSimArr, $threshold);
		// preson($matrix);
		preson($contextualSimArr);
		$this->setContextualSimArr($contextualSimArr);
		preout("x ∩ y " . $cntThres);

		$pathLength = 0;

		
		// preson("path val: ");
		// preson("path len: ".$pathLength);
		/*
		Path method ( scraping )
		*/
		/*
		susunan kata task activity sesuai jenis noun & verb
		mencari kemiripan kata pada sinonimkata.com
		matrix kemiripan data 
		menentukan nilai tertinggi pada hasil kemiripan kata sesuai jenis noun & verb
		hitung rata2 tiap kelompok nilai pada jenis noun & verb
		nilai kemiripan kalimat = hitung kemiripan kalimat dengan nilai bobot noun & verb
		*/
		return $cntThres;
	}

	public function connectorSimmilarity(
		$connArr1 = [],
		$connArr2 = []
	){
		$connArr1 = [1,2,3,4,5,6,7];
		$connArr2 = [1,2,3,4,7,7];
		
		preson( $connArr1);
		preson( $connArr2);
		// x n y
		$intersect1 = array_intersect($connArr1, $connArr2);
		preson( $intersect1);
		$connectorSim = sizeof($intersect1);
		preout("x n y : ".$connectorSim);

		return (int) $connectorSim;
	}

	public function branchSimmilarity(){
		$branchSim = 0;
		$type = 1;
		$makna = 1;
		$kondisi = 1;
		$jumlah = 1;

		$branchSim = $type + $makna +
			$kondisi + $jumlah;
		$branchSim = $branchSim / 4;

		preout($branchSim);

		return (int) $branchSim;
	}

	public function structuralSimmilarity($semSim = 0){
		preout(__METHOD__." struct sim measure");
		preout("Data : startA A AB B BR1 R1 R1C C CB R1end");
		/*
		mp1, mp2 = model proses 1 & 2
		hitung jumlah kesamaan 
			label task, connector, percabangan pada (mp1, mp2) 
		nilai kemiripan = hitung kemiripan

		*/
		
		$connectorSim = $this->connectorSimmilarity();
		$branchSim = $this->branchSimmilarity();
		
		preout("$semSim + $connectorSim + $branchSim");
		$structuralSim = $semSim + $connectorSim + $branchSim;

		return $structuralSim;
	}

	public function jaccard($xny = 1, $xuy = 1){
		return $xny / $xuy;
	}

	// semantic & structural simmilarity
	// jaccard algo
	public function simmilarityMeasure(){
		preout(__METHOD__." sim measure");
		$semSim = $this->semanticSimmilarity();
		$structSim = $this->structuralSimmilarity($semSim);

		preout("semSim : ".$semSim);
		preout("structSim : ".$structSim);
		

		$xny = $structSim;
		// to do : make dynamic
		$xuy = 12 + 10 - $xny;

		$jaccard = $this->jaccard($xny, $xuy);
		preout("jaccard Sim : ".$jaccard);
	}

	public function cluster(){
		$threshold = 0.27;

		// retrieve from db
		$structuralSimArr = [
		];
		
		/*
			sss
		*/
	}

	public function commonFragmentExtraction(){
		$threshold = 0.27;

		// retrieve from db
		$contextualSimArr = $this->getContextualSimArr();
		
		/*
			sss
		*/
	}

	public function table($fill = ""){
		return "<table border=\"2px\">$fill</table>";
	}

	public function td($fill = ""){
		return "<td>$fill</td>";
	}

	public function tr($fill = ""){
		return "<tr>$fill</tr>";
	}

	public function output(){
		// 3.4
		$arr = [
			"tokped",
			"bukalapak",
			"shopee",
		];

		$out = "";
		$tr = $this->td("=");
		foreach($arr as $a){
			$tr .= $this->td("$a");
		}
		$out .= $this->tr($tr);
		foreach($arr as $a){
			$r = "";
			foreach($arr as $a2){
				// enumerate results
				$r .= $this->td("$a, $a2");
			}
			$out .= $this->tr($this->td("$a").$r);
		}
		echo $this->table($out);
	}

	public function full(){
		preout(__METHOD__." full");
		// $this->parseData()
		// pemodelan & identifikasi 
		// $this->parseData()
		
		// menghitung kemiripan
		$this->simmilarityMeasure();
		// clustering
		$this->cluster();
		// ekstraksi common fragment
		$this->commonFragmentExtraction();
		
		$this->output();
	}

	public function finalResult($type = "brief"){
		$q = "";
		// to do : view

		switch($type){
			case "full":
				$q = "";
			break;
			default:
				$q = "";
		}
		
		return $this->db->query($q);
	}
}