<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__.'/Base/BaseModel.php');

class ResultModel extends BaseModel {
	public $table = "res_contextual";

	public function baseQuery($type = ""){
		$q = "";
		switch($type){
			// case "final-result":
			// break;
			case "rpa":
				$q = "SELECT rf.*,rf2.*, (
					select if(rf2.cluster != 'uncluster',
						if(rf.sim_overall >= 0.27,'TP',
						'FP')
						, if(rf.sim_overall >= 0.27,'FP',
						'TN')
					)
				) as acc, rf.id as cnt
				 FROM res_final rf
				left join (
					select (if(p.nama = p2.nama, p.nama, 'uncluster')) as cluster, 
						c.nama as cnama, c2.nama as c2nama, 
						p.nama as pnama, p2.nama as p2nama, rfi.id 
					FROM res_final rfi
					left join proses p on rfi.idproses = p.id 
					left join proses p2 on rfi.idproses2 = p2.id
					left join commerce c on p.idcommerce = c.id
					left join commerce c2 on p2.idcommerce = c2.id
				) rf2 on rf2.id = rf.id";
			break;
			case "rpa-group":
				// , count(rf.id) as cnt
				$q = $this->baseQuery("rpa");
				$q = str_replace("(
					select if(rf2.cluster != 'uncluster',
						if(rf.sim_overall >= 0.27,'TP',
						'FP')
						, if(rf.sim_overall >= 0.27,'FP',
						'TN')
					)
				) as acc, rf.id as cnt", "rpa.val as acc, count(rf.id) as cnt", $q);
				$q = str_replace("FROM res_final rf
				left join", "FROM rpa 
				left join res_final rf on rpa.val = rf.acc
				left join", $q);
				$q = $q." 
				group by rpa.val";
			break;
			case "final-result":
				$q = "SELECT rf.id, c.nama as cnama, c2.nama as c2nama, p.nama as pnama, p2.nama as p2nama, 
				p.cnt_element as pelcnt, p2.cnt_element as p2elcnt, rf.intersects,
				(rf.intersects / ( p.cnt_element + p2.cnt_element - rf.intersects ) ) as jsim, rf.sim_overall
				FROM res_final rf
				join proses p on rf.idproses = p.id
				join proses p2 on rf.idproses2 = p2.id
				join commerce c on p.idcommerce = c.id
				join commerce c2 on p2.idcommerce = c2.id";
			break;
			case "contextual-sim":
				$q = "SELECT rctx.id, c.nama as cnama, 
				c2.nama as c2nama, sp.nama as spnama, sp2.nama as sp2nama, 
				rctx.ctx_sim, 
				( rctx.nmax * 0.25 ) + ( rctx.vmax * 0.75 ) as ctxsimcalc 
				FROM res_contextual rctx
				join commerce c on rctx.idcommerce = c.id
				join commerce c2 on rctx.idcommerce2 = c2.id
				join subproses sp on rctx.idsubproses = sp.id
				join subproses sp2 on rctx.idsubproses2 = sp2.id";
			break;
			case "common-fragment":
			default:
				$q = $this->baseQuery("contextual-sim");
				$q .= " where ctx_sim >= 0.25";
		}
		return $q;
	}

	public function getAll(){
		$q = "SELECT rctx.id, rctx.*, ( rctx.nmax * 0.25 ) + ( rctx.vmax * 0.75 ) as ctxsimcalc FROM res_contextual rctx";
		return $this->db->query($q)
			->result();
	}

	public function commonFragment(){
		$q = $this->baseQuery("common-fragment");
		return $this->db->query($q)
			->result();
	}

	public function getRecall($tp, $fn){
		return $tp / ($tp + $fn);
	}

	public function getPrecision($tp, $fp){
		return $tp / ($tp + $fp);
	}

	public function getAccuration($tp, $tn, $fp, $fn){
		return ($tp + $tn) / ($tp + $tn + $fp + $fn);
	}

	public function getRPAs($tp, $tn, $fp, $fn){
		return [
			"recall" => $this->getRecall($tp, $fn),
			"precision" => $this->getPrecision($tp, $fp),
			"accuration" => $this->getAccuration($tp, $tn, $fp, $fn)
		];
	}

	public function getRPA(){
		$q = $this->baseQuery("rpa-group");
		$res = $this->db->query($q)->result();
		// preout($res);
		$params = [];
		foreach($res as $r){
			$params[$r->acc] = $r->cnt;
		}
		// preout($params);

		$tp = $params['TP'];
		$tn = $params['TN'];
		$fp = $params['FP'];
		$fn = $params['FN'];

		return $this->getRPAs($tp, $tn, $fp, $fn) ;
	}
}