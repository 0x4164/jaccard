<?php
require_once(__DIR__.'/../Base/BaseDatatable.php');
// datatable
class DataSinonim extends BaseDatatable{
    protected $table = "mdl_course";
	protected $tableView = "mdl_course mc";
	
	protected $select = "*";
	protected $column_search = ['fullname'];
	protected $column_order = array(null, null, 'kriteria', 'id_dinas');
	protected $order = array('id' => 'desc');
	
	const ALL = "SELECT s.*, c.nama as c1n, c2.nama as c2n 
		from sinonim s
		left join commerce c on s.idcommerce = c.id
		left join commerce c2 on s.idcommerce2 = c2.id
		";
	
    public function __construct(){
        
	}

	// @return array
	public function getCustomSearchValue(){
		$ret = [];
		return $ret;
	}

	public function search(){
		// $q = $this->getSearchValue();
		$ret = [];

        return $ret;
    }
	
	function getRendered(){
		$list = $this->get(self::ALL);
		$data = array();
		$no = $this->input->post('start');

		foreach ($list->data as $p) {
			$no++;
			$row = [];
			$row['id'] = $p->id;
			$row['idcommerce'] = $p->idcommerce;
			$row['idcommerce2'] = $p->idcommerce2;
			$row['c1n'] = $p->c1n;
			$row['c2n'] = $p->c2n;

			$row['idsubproses'] = $p->idsubproses;
			$row['idsubproses2'] = $p->idsubproses2;
			$row['subp'] = $p->subp;
			$row['subp2'] = $p->subp2;
			$row['w1'] = $p->w1;
			$row['w2'] = $p->w2;
			$row['wx'] = $p->wx;
			$row['wsinonim'] = $p->wsinonim;
			$row['action'] = "";
			$data[] = $row;
		}
		$this->_datatable_output($data, $list->recordsTotal, $list->recordsFiltered);
	}
}