<?php
require_once(__DIR__.'/../Base/BaseDatatable.php');
// datatable
class DataResFinal extends BaseDatatable{
    protected $table = "mdl_course";
	protected $tableView = "mdl_course mc";
	
	protected $select = "*";
	protected $column_search = ['fullname'];
	protected $column_order = array(null, null, 'kriteria', 'id_dinas');
	protected $order = array('id' => 'desc');
	
	const ALL = "SELECT * from proses";
	
    public function __construct(){
        $this->load->model('ResultModel', 'resultmodel');
	}

	// @return array
	public function getCustomSearchValue(){
		$ret = [];
		return $ret;
	}

	public function search(){
		// $q = $this->getSearchValue();
		$ret = [];

        return $ret;
    }
	
	function getRendered(){
		$list = $this->get(
			$this->resultmodel->baseQuery("final-result")
		);
		$data = array();
		$no = $this->input->post('start');

		foreach ($list->data as $p) {
			$no++;
			$row = [];
			$row['id'] = $p->id;
			$row['cnama'] = $p->cnama;
			$row['c2nama'] = $p->c2nama;
			$row['pnama'] = $p->pnama;
			$row['p2nama'] = $p->p2nama;
			$row['pelcnt'] = $p->pelcnt;
			$row['p2elcnt'] = $p->p2elcnt;
			$row['jsim'] = $p->jsim;
			$row['sim_overall'] = $p->sim_overall;
			$row['action'] = "";
			$data[] = $row;
		}
		$this->_datatable_output($data, $list->recordsTotal, $list->recordsFiltered);
	}
}