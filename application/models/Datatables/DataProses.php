<?php
require_once(__DIR__.'/../Base/BaseDatatable.php');
// datatable
class DataProses extends BaseDatatable{
    protected $table = "mdl_course";
	protected $tableView = "mdl_course mc";
	
	protected $select = "*";
	protected $column_search = ['fullname'];
	protected $column_order = array(null, null, 'kriteria', 'id_dinas');
	protected $order = array('id' => 'desc');
	
	const ALL = "SELECT * from proses";
	
    public function __construct(){
        
	}

	// @return array
	public function getCustomSearchValue(){
		$ret = [];
		return $ret;
	}

	public function search(){
		// $q = $this->getSearchValue();
		$ret = [];

        return $ret;
    }
	
	function getRendered(){
		$list = $this->get(self::ALL);
		$data = array();
		$no = $this->input->post('start');

		foreach ($list->data as $p) {
			$no++;
			$row = [];
			$row['id'] = $p->id;
			$row['idcommerce'] = $p->idcommerce;
			$row['symbol'] = $p->symbol;
			$row['nama'] = $p->nama;
			$row['action'] = "";
			$data[] = $row;
		}
		$this->_datatable_output($data, $list->recordsTotal, $list->recordsFiltered);
	}
}