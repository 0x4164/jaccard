<?php
require_once(__DIR__.'/../Base/BaseDatatable.php');
// datatable
class DataRPA extends BaseDatatable{
    protected $table = "mdl_course";
	protected $tableView = "mdl_course mc";
	
	protected $select = "*";
	protected $column_search = ['fullname'];
	protected $column_order = array(null, null, 'kriteria', 'id_dinas');
	protected $order = array('id' => 'desc');
	
	const ALL = "select 1";
	
    public function __construct(){
        $this->load->model('ResultModel', 'resultmodel');
	}

	// @return array
	public function getCustomSearchValue(){
		$ret = [];
		return $ret;
	}

	public function search(){
		// $q = $this->getSearchValue();
		$ret = [];

        return $ret;
    }
	
	function getRendered(){
		$query = $this->resultmodel->baseQuery("rpa");
		$useGroup = arrayGet($_GET, "rpa-group")=="1";
		if($useGroup){
			$query = $this->resultmodel->baseQuery("rpa-group");
		}
		$list = $this->get($query);
		
		$data = array();
		$no = $this->input->post('start');

		foreach ($list->data as $p) {
			$no++;
			$row = [];
			$row['id'] = $p->id;
			$row['cnama'] = $p->cnama;
			$row['c2nama'] = $p->c2nama;
			$row['sim_overall'] = $p->sim_overall;
			$row['cluster'] = $p->cluster;
			$row['acc'] = $p->acc;
			$row['cnt'] = $p->cnt;
			$row['action'] = "";
			$data[] = $row;
		}
		$this->_datatable_output($data, $list->recordsTotal, $list->recordsFiltered);
	}
}