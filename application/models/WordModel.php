<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__.'/Base/BaseModel.php');

class WordModel extends BaseModel {
	public $table = "word";

	public function getAllWord(){
		return $this->db->select("distinct kata", false)
			->from("word")->get();
	}
}